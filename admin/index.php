<?php

include_once ("../config.php");

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Clinical trials stopped by Covid-19</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- Stopped Covid-19 trials CSS -->
	<link href="<?php echo SITE_URL; ?>covid-19.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>Admin: Clinical trials stopped by Covid-19</h1>

		    <p>Remaining to be screened: <?php echo count_trials_not_assessed_for_inclusion(); ?></p>

		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <th scope="col">NCT ID</th>
			    <th scope="col">Archives</th>
			    <th scope="col">Auto</th>
			    <th scope="col">Overall status</th>
			    <th scope="col">Why stopped</th>
			    <th scope="col">Last update</th>
			    <th scope="col">Include</th>
			    <th scope="col">Covid-19 explicit</th>
			    <th scope="col">Restart expected</th>
			    <th scope="col">Date stopped</th>
			</thead>
			<tbody>
			    <?php

			    $rows = get_admin_table_entries ();

			    foreach ( $rows as $row ) {

				switch ($row['overall_status']) {
				    case "Terminated":
					$row_css = "danger";
					break;
				    case "Suspended":
					$row_css = "warning";
					break;
				    case "Withdrawn":
					$row_css = "info";
					break;
				}

			    ?><tr>
			    <th scope="row"><a href="https://clinicaltrials.gov/ct2/show/<?php echo $row['nct_id']; ?>" target="_blank"><?php echo $row['nct_id']; ?></a></th>
			    <td><a href="https://clinicaltrials.gov/ct2/history/<?php echo $row['nct_id']; ?>" target="_blank">Changes</a></td>
			    <td><button onclick="auto_check_ctg (<?php echo $row['id']; ?>, '<?php echo $row['nct_id']; ?>');" class="btn btn-sm btn-primary">Autocheck</button></td>
			    <td><span class="badge badge-<?php echo $row_css; ?>"><?php echo $row['overall_status']; ?></span></td>
			    <td><?php echo $row['why_stopped']; ?></td>
			    <td onclick="$('#datestopped-<?php echo $row['id']; ?>').val('<?php echo $row['last_update_submitted']; ?>').trigger('input');"><?php echo $row['last_update_submitted']; ?></td>
			    <?php

			    // Include row

			    if ( $row['include'] === NULL ) {
				echo "<td id=\"include-" . $row['id'] . "\" onclick=\"click_include(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['include']) {
				    case 0:
					echo "<td id=\"include-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_include(" . $row['id'] . ");\">No</td>";
					break;
				    case 1:
					echo "<td id=\"include-" . $row['id'] . "\" class=\"table-success\" onclick=\"click_include(" . $row['id'] . ");\">Yes</td>";
					break;
				}
			    }
			    
			    // Covid-19 explicit row

			    if ( $row['covid19_explicit'] === NULL ) {
				echo "<td id=\"covid-" . $row['id'] . "\" onclick=\"click_covid(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['covid19_explicit']) {
				    case 0:
					echo "<td id=\"covid-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_covid(" . $row['id'] . ");\">No</td>";
					break;
				    case 1:
					echo "<td id=\"covid-" . $row['id'] . "\" class=\"table-success\" onclick=\"click_covid(" . $row['id'] . ");\">Yes</td>";
					break;
				}
			    }
			    
			    // Restart expected row

			    if ( $row['restartexpected'] === NULL ) {
				echo "<td id=\"restartexpected-" . $row['id'] . "\" onclick=\"click_restartexpected(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['restartexpected']) {
				    case 0:
					echo "<td id=\"restartexpected-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_restartexpected(" . $row['id'] . ");\">No</td>";
					break;
				    case 1:
					echo "<td id=\"restartexpected-" . $row['id'] . "\" class=\"table-success\" onclick=\"click_restartexpected(" . $row['id'] . ");\">Yes</td>";
					break;
				}
			    }
			    
			    ?>
			    <td>
				<div class="input-group">
				    <div class="input-group-prepend">
					<span class="input-group-text" id="date_prepend">YYYY-MM-DD</span>
				    </div>
				    <input type="text" id="datestopped-<?php echo $row['id']; ?>" trial_id="<?php echo $row['id']; ?>" class="form-control date-stopped" placeholder="-" aria-label="Date" aria-describedby="date_prepend" value="<?php echo $row['date_stopped']; ?>">
				</div>
			    </td>
			    </tr><?php
				 
				 }

				 ?>
			</tbody>
		    </table>

		    <a href="<?php echo SITE_URL; ?>admin/" class="btn btn-primary btn-block" style="margin-bottom: 40px;">More</a>
		</div>
	    </div>
	</div>
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- Stopped Covid-19 trials JS -->
	<script>
	 function click_include (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/include.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error') {

		     $('#include-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#include-' + trial_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#include-' + trial_id).removeClass('table-success');
			     $('#include-' + trial_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#include-' + trial_id).removeClass('table-danger');
			     $('#include-' + trial_id).addClass('table-success');
			     break;
		     }
		 }
		 
	     });
	     
	 }

	 function click_covid (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/covid.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error') {

		     $('#covid-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#covid-' + trial_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#covid-' + trial_id).removeClass('table-success');
			     $('#covid-' + trial_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#covid-' + trial_id).removeClass('table-danger');
			     $('#covid-' + trial_id).addClass('table-success');
			     break;
		     }
		 }
		 
	     });

	 }
	     

	 function click_restartexpected (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/restartexpected.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error') {

		     $('#restartexpected-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#restartexpected-' + trial_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#restartexpected-' + trial_id).removeClass('table-success');
			     $('#restartexpected-' + trial_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#restartexpected-' + trial_id).removeClass('table-danger');
			     $('#restartexpected-' + trial_id).addClass('table-success');
			     break;
		     }
		 }
		 
	     });
	     
	 }

	     function auto_check_ctg (trial_id, nct_id) {

		 $.ajax ({
		     url: '<?php echo SITE_URL; ?>' + 'admin/ctg-history-check.php',
		     type: 'post',
		     data: {
			 nctid: nct_id
		     },
		     dataType: 'html'
		 }).done ( function (response) {

		     if ( response == 'Exclude' ) {

			 $('#include-' + trial_id).click();
			 $('#include-' + trial_id).click();
			 
		     } else {

			 $('#include-' + trial_id).click();
			 $('#datestopped-' + trial_id).val(response);
			 $('#datestopped-' + trial_id).trigger('input');
		     }
		     
		 });
		 
	     }

	     $('.date-stopped').on('input', function (e) {
		 selected_input = $(this);
		 trial_id = $(this).attr('trial_id');
		 newdate = $(this).val();

		 if (newdate == '') {
		     $.ajax ({
			 url: '<?php echo SITE_URL; ?>' + 'admin/clear-datestopped.php',
			 type: 'post',
			 data: {
			     tid: trial_id
			 },
			 dataType: 'html'
		     }).done ( function (response) {

			 if ( response == '-') {

			     selected_input.parent().parent().addClass('table-success');

			     setTimeout(
				 function () {
				     selected_input.parent().parent().removeClass('table-success');
				 },
				 1000
			     );
			 }

		     });
		     
		 }

		 if (/^([0-9]{4,})-([0-9]{2,})-([0-9]{2,})$/.test(newdate)) {
		     
		     $.ajax ({
			 url: '<?php echo SITE_URL; ?>' + 'admin/datestopped.php',
			 type: 'post',
			 data: {
			     tid: trial_id,
			     nd: newdate
			 },
			 dataType: 'html'
		     }).done ( function (response) {

			 if ( response != 'MySQL Error') {

			     if ( response != 'nwf' ) {
				 // well-formed date
				 selected_input.parent().parent().addClass('table-success');
				 
				 setTimeout(
				     function () {
					 selected_input.parent().parent().removeClass('table-success');
					 selected_input.val(response);
				     },
				     1000
				 );
			     } else {
				 // non-well-formed date
				 selected_input.parent().parent().addClass('table-danger');
				 
				 setTimeout(
				     function () {
					 selected_input.parent().parent().removeClass('table-danger');
					 selected_input.val('');
				     },
				     1000
				 );
			     }
			 }

		     });
		 }

	     });
	</script>
    </body>
</html>
