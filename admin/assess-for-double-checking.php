<?php

// So this script was written for something that didn't end up being useful

include_once ("../config.php");

$date_of_interest = "2017-12-01";
$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$trials = get_excluded_and_non_double_checked_comparators ( 25 );

foreach ($trials as $trial) {

    echo $trial['nct_id'] . "<br>\n";

    $nct_id = $trial['nct_id'];

    $ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;

    $remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

    if ( $remote_index_file ) {

	$stopped = FALSE;

	echo "Loaded index<br><br>\n\n";

	// This extracts a date for each of the versions of the registry entry
	$regex = '/>([A-Za-z0-9, ]+)<\/a>/';
	
	preg_match_all (
	    $regex,
	    $remote_index_file,
	    $dates,
	    PREG_UNMATCHED_AS_NULL
	);

	$sections = preg_split(
	    $regex,
	    $remote_index_file,
	    -1, // no limit
	    PREG_SPLIT_NO_EMPTY
	);

	foreach ($dates[1] as $version_index=>$version_date) {

	    // echo $version_date;

	    $version_index = $version_index++;

	    if ( strtotime($version_date) > strtotime($date_of_interest) ) {
		// If this version is after the date of interest ...

		if ( strpos($sections[$version_index+1], " --> Suspended") | strpos($sections[$version_index+1], " --> Terminated") | strpos($sections[$version_index+1], " --> Withdrawn") ) {

		    echo "Stopped on " . $version_date . "<br>\n";

		    $stopped = TRUE;
		    
		}
	    }
	}

	if ( $stopped ) {
	    mark_for_double_checking ( $trial['id'], 1 );
	} else {
	    mark_for_double_checking ( $trial['id'], 0 );
	}
	
    } else {
	echo "<p>Could not load the index file for " . $nct_id . "</p>";
    }
    
}

if ( count ($trials) > 0 ) {
?><script>
   setTimeout(function () {
       location.reload();
   }, 0); // Set this to a number of thousandths of a second to wait between refreshes
</script><?php
}

?>
