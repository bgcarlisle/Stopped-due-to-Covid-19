<?php

// Go to the address for this PHP file in your browser and add the following:
// ?start=2018-12-01&end=2018-12-02
// And it will start working

include_once ("../config.php");

$success = FALSE;

$days_at_a_time = 2;

echo "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=" . date("m/d/Y", strtotime($_GET['start'])) . "&lupd_e=" . date("m/d/Y", strtotime($_GET['end'])) . "<br><br>";

if ( denominator_download ($_GET['start'], $_GET['end']) ) {
    $success = TRUE;
} else {
    $success = FALSE;
}

if ( ! $success ) {
?><script>
 setTimeout(function () {
     window.location = '<?php echo SITE_URL; ?>admin/populate_denominator.php?start=<?php echo $_GET['start']; ?>&end=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400); ?>';
 }, 1000);
</script><?php
} else {
	 ?><script>
	    setTimeout(function () {
		//window.location = '<?php echo SITE_URL; ?>admin/populate_denominator.php?start=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400) ?>&end=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400*$days_at_a_time) ?>';
	    }, 3000);
	    // alert ('it worked');
	 </script><?php
}

?>
