<?php

include_once ("config.php");

$trial = get_trial_details ($_POST['tid']);

$sponsors = get_trial_sponsors ($_POST['tid']);

$interventions = get_trial_interventions ($_POST['tid']);

$indications = get_trial_indications ($_POST['tid']);

echo json_encode (array($trial, $sponsors, $interventions, $indications));

?>
