library(tidyverse)
library(rvest)

### This script takes a CSV file with a column called "nctid"
nctids <- read_csv("2021-02-07-unique-missing-ids.csv")

### The output of this script is written to a new CSV called
### historical_versions.csv
new_row <- data.frame(
    "nctid",
    "version_number",
    "version_date",
    "overall_status",
    "study_start_date",
    "primary_completion_date",
    "primary_completion_date_type",
    "enrolment",
    "enrolment_type"
)
write_csv(new_row, "historical_versions.csv", append=TRUE)

download_index <- function (nctid) {
    out <- tryCatch({

        url <- paste0(
            "https://clinicaltrials.gov/ct2/history/",
            nctid
        )

        index <- read_html(url)

        index %>%
            html_nodes("fieldset.releases table a") %>%
            html_text() %>%
            as.Date(format="%B %d, %Y") %>%
            format("%Y-%m-%d") %>%
            return()

    },
    error=function(cond) {
        message(paste("NCT ID does not seem to exist:", nctid))
        message("Here's the original error message:")
        message(cond)
                                        # Choose a return value in case of warning
        return (NA)
    },
    warning=function(cond) {
        message(paste("NCT ID caused a warning:", nctid))
        message("Here's the original warning message:")
        message(cond)
                                        # Choose a return value in case of warning
        return(NULL)
    },
    finally={
                                        # To execute regardless of success or failure
    })
    
    return(out)
    
}

download_version_data <- function (nctid, versionno) {
    out <- tryCatch({

        url <- paste0(
            "https://clinicaltrials.gov/ct2/history/",
            nctid,
            "?V_",
            versionno
        )

        version <- read_html(url)

        ## Read the overall status

        ostatus_rows <- version %>%
            html_nodes("#StudyStatusBody tr") %>%
            html_text() %>%
            str_replace_all("\n", " ") %>%
            str_replace_all("[  ]+", " ") %>%
            trimws()

        ostatus <- NA
        for (ostatus_row in ostatus_rows) {

            ostatus_row <- ostatus_row %>%
                str_extract("Overall Status: ([A-Za-z ]+)")
            
            if ( ! is.na (ostatus_row) ) {
                ostatus <- sub("Overall Status: ([A-Za-z ]+)", "\\1", ostatus_row)
            }
        }

        ## Read the enrolment and type

        enrol_rows <- version %>%
            html_nodes("#StudyDesignBody tr") %>%
            html_text() %>%
            str_replace_all("\n", " ") %>%
            str_replace_all("[  ]+", " ") %>%
            trimws()

        enrol <- NA
        for (enrol_row in enrol_rows) {

            enrol_row <- enrol_row %>%
                str_extract("Enrollment: ([A-Za-z0-9 \\[\\]]+)")

            if ( ! is.na (enrol_row) ) {
                enrol <- sub("Enrollment: ([A-Za-z0-9]+)", "\\1", enrol_row)
            }
        }

        ## Read the study start date

        startdate_rows <- version %>%
            html_nodes("#StudyStatusBody tr") %>%
            html_text() %>%
            str_replace_all("\n", " ") %>%
            str_replace_all("[  ]+", " ") %>%
            trimws()

        startdate_raw <- NA
        
        for (startdate_row in startdate_rows) {
            startdate_row <- startdate_row %>%
                str_extract("Study Start: ([A-Za-z0-9, ]+)")

            if ( ! is.na (startdate_row) ) {
                startdate_raw <- sub("Study Start: ([A-Za-z0-9, ]+)", "\\1", startdate_row)
            }
        }

        startdate_full <- startdate_raw %>%
            as.Date(format="%B %d, %Y") %>%
            format("%Y-%m-%d")

        startdate_month <- startdate_raw %>%
            paste(1) %>%
            as.Date(format="%B %Y %d") %>%
            format("%Y-%m-%d")

        if ( ! is.na (startdate_full) ) {
            startdate <- startdate_full
        } else {
            startdate <- startdate_month
        }

        ## Read the primary completion date

        pcdate_rows <- version %>%
            html_nodes("#StudyStatusBody tr") %>%
            html_text() %>%
            str_replace_all("\n", " ") %>%
            str_replace_all("[  ]+", " ") %>%
            trimws()

        pcdate_raw <- NA
        
        for (pcdate_row in pcdate_rows) {
            pcdate_row <- pcdate_row %>%
                str_extract("Primary Completion: ([A-Za-z0-9, \\[\\]]+)")

            if ( ! is.na (pcdate_row) ) {
                pcdate_raw <- sub("Primary Completion: ([A-Za-z0-9, ]+)", "\\1", pcdate_row)
            }
        }

        pcdate_full <- pcdate_raw %>%
            str_extract("[A-Za-z0-9 ,]+") %>%
            trimws() %>%
            as.Date(format="%B %d, %Y") %>%
            format("%Y-%m-%d")

        pcdate_month <- pcdate_raw %>%
            str_extract("[A-Za-z0-9 ,]+") %>%
            trimws() %>%
            paste(1) %>%
            as.Date(format="%B %Y %d") %>%
            format("%Y-%m-%d")

        if ( ! is.na (pcdate_full) ) {
            pcdate <- pcdate_full
        } else {
            pcdate <- pcdate_month
        }

        pcdatetype <- pcdate_raw %>%
            str_extract("\\[[A-Za-z]+\\]") %>%
            str_extract("[A-Za-z]+")

        ## Now, put all these data points together

        data <- c(
            ostatus,
            enrol,
            startdate,
            pcdate,
            pcdatetype
        )

        return(data)
        
    },
    error=function(cond) {
        message(paste("Version does not seem to exist:", nctid, "version", versionno))
        message("Here's the original error message:")
        message(cond)
                                        # Choose a return value in case of error
        return (NA)
    },
    warning=function(cond) {
        message(paste("Version caused a warning:", nctid, "version", versionno))
        message("Here's the original warning message:")
        message(cond)
                                        # Choose a return value in case of warning
        return(NULL)
    },
    finally={
                                        # To execute regardless of success or failure
    })
    
}

nct_count <- 0

for (nctid in nctids$nctid) {

    versions <- download_index (nctid)

    versionno <- 1;
    for (version in versions) {
        
        versiondata <- download_version_data(nctid, versionno)

        overall_status <- versiondata[1]
        
        enrol <- versiondata[2]
        enrolno <- enrol %>%
            str_extract("^[0-9]+")
        enroltype <- enrol %>%
            str_extract("[A-Za-z]+")

        startdate <- versiondata[3]

        pcdate <- versiondata[4]
        
        pcdatetype <- versiondata[5]
        
        new_row <- data.frame(
            nctid,
            versionno,
            version,
            overall_status,
            startdate,
            pcdate,
            pcdatetype,
            enrolno,
            enroltype
        )
        write_csv(new_row, "historical_versions.csv", append=TRUE)
        
        versionno <- versionno + 1
    }

    nct_count <- nct_count + 1
    progress <- format(100 * nct_count / nrow(nctids), digits=2)

    message(paste0(nctid, " processed (", progress, "%)"))
    
}
