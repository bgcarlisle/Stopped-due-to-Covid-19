library(tidyverse)

missing <- read_csv("2021-02-07-unique-missing-ids.csv")

hv <- read_csv("historical_versions.csv")

hv$remove <- hv$nctid %in% missing$nctid

hv <- hv %>%
    filter (! remove) %>%
    select (! remove)

hv %>% write_csv("historical_versions.csv")
