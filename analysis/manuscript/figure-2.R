library(tidyverse)
library(ggplot2)

## Read the covid arm TSV
c19trials <- read_delim(
  "../data/covid-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Read the comparator arm TSV
comptrials <- read_delim(
  "../data/comparator-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Only look at interventional trials
c19trials <- c19trials %>%
    filter(study_type == "Interventional")

comptrials <- comptrials %>%
    filter(study_type == "Interventional")

## Remove data points from outside 1-year sample
c19trials <- c19trials %>%
    filter(date_stopped < as.Date("2020-12-01"))

## Sort by date
c19trials <- c19trials %>%
    arrange(date_stopped)

comptrials <- comptrials %>%
    arrange(date_stopped)

## Only consider trials with an actual enrolment
## greater than zero
c19trials <- c19trials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

comptrials <- comptrials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

## Move the dates in the comparator forward

comptrials$date_stopped <- comptrials$date_stopped + 365*2

## Add a column for cumulative enrolment
c19trials$cumsumenrol <- cumsum(c19trials$enrollment)

comptrials$cumsumenrol <- cumsum(comptrials$enrollment)

## Make a subset that includes only trials stopped for reasons
## that explicitly cite Covid-19
c19e <- c19trials %>%
    filter(covid19_explicit == 1)

## Add a column for cumulative enrolment
c19e$cumsumenrol <- NULL
c19e$cumsumenrol <- cumsum(c19e$enrollment)

## Generate plot
fig1 <- ggplot(
    aes(
        x = date_stopped,
        y = cumsumenrol,
        linetype = "Stopped during Covid-19 pandemic (Dec 2019 to Nov 2020, any reason)"
    ),
    data = c19trials
) +
    geom_line() +
    scale_x_date(
        breaks = "1 month",
        minor_breaks = NULL,
        date_labels = "%b"
    ) +
    labs(
        x = "Date trial stopped",
        y = "Cumulative number of enrolled patients",
        linetype = "Arm"
    ) +
    geom_line(
        aes(
            x = date_stopped,
            y = cumsumenrol,
            linetype = "Stopped in comparator (Dec 2017 to Nov 2018, any reason)"
        ),
        data = comptrials
    ) +
    geom_line(
        aes(
            x = date_stopped,
            y = cumsumenrol,
            linetype = "Stopped for reasons citing Covid-19 (Dec 2019 to Nov 2020)"
        ),
        data = c19e
    ) +
    scale_linetype_manual(
        breaks = c(
            "Stopped during Covid-19 pandemic (Dec 2019 to Nov 2020, any reason)",
            "Stopped for reasons citing Covid-19 (Dec 2019 to Nov 2020)",
            "Stopped in comparator (Dec 2017 to Nov 2018, any reason)"
        ),
        values = c(1,2,3)
    ) +
    theme(
        legend.position="bottom",
        legend.direction="vertical"
    )

pdf(
    "fig2.pdf",
    width=8,
    height=6
)

fig1

dev.off()

png(
    "fig2.png",
    width=8,
    height=6,
    units="in",
    res=200
)

fig1

dev.off()

c19e %>%
    arrange(enrollment) %>%
    select(nct_id, enrollment, enrollment_type) %>%
    tail()
