<?php

function process_xml_file ( $filename ) {

    $xml = simplexml_load_file($filename);

    if (! $xml ) {

        echo "The XML file didn't load<br>";

    } else {

        echo "The XML file loaded okay<br>";

        echo $xml->id_info->nct_id . "<br>";

    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("INSERT INTO `trials` (`nct_id`, `overall_status`, `why_stopped`, `primary_completion_date`, `primary_completion_date_type`, `last_update_submitted`, `brief_title`, `official_title`, `brief_summary`, `detailed_description`, `enrollment_type`, `enrollment`, `phase`, `study_type`, `allocation`, `intervention_model`, `primary_purpose`, `masking`) VALUES (:nct_id, :overall_status, :why_stopped, :primary_completion_date, :primary_completion_date_type, :last_update_submitted, :brief_title, :official_title, :brief_summary, :detailed_description, :enrollment_type, :enrollment, :phase, :study_type, :allocation, :intervention_model, :primary_purpose, :masking)");

        $stmt->bindParam(':nct_id', $nct_id);
        $stmt->bindParam(':overall_status', $overall_status);
        $stmt->bindParam(':why_stopped', $why_stopped);
        $stmt->bindParam(':primary_completion_date', $primary_completion_date);
        $stmt->bindParam(':primary_completion_date_type', $primary_completion_date_type);
        $stmt->bindParam(':last_update_submitted', $last_update_submitted);
        $stmt->bindParam(':brief_title', $brief_title);
        $stmt->bindParam(':official_title', $official_title);
        $stmt->bindParam(':brief_summary', $brief_summary);
        $stmt->bindParam(':detailed_description', $detailed_description);
        $stmt->bindParam(':enrollment_type', $enrollment_type);
        $stmt->bindParam(':enrollment', $enrollment);
        $stmt->bindParam(':phase', $phase);
        $stmt->bindParam(':study_type', $study_type);
        $stmt->bindParam(':allocation', $allocation);
        $stmt->bindParam(':intervention_model', $intervention_model);
        $stmt->bindParam(':primary_purpose', $primary_purpose);
        $stmt->bindParam(':masking', $masking);

        $nct_id = $xml->id_info->nct_id;
        $overall_status = $xml->overall_status;

        if ($xml->why_stopped != "") {
            $why_stopped = $xml->why_stopped;
        } else {
            $why_stopped = NULL;
        }

        if ( isset ($xml->primary_completion_date) ) {
            $primary_completion_date = date("Y-m-d", strtotime($xml->primary_completion_date));
            $primary_completion_date_type = $xml->primary_completion_date["type"];
        } else {
            $primary_completion_date = NULL;
            $primary_completion_date_type = NULL;
        }

        $last_update_submitted = date("Y-m-d", strtotime($xml->last_update_submitted));
        $brief_title = $xml->brief_title;
        $official_title = $xml->official_title;

        $brief_summary_string = "";
        foreach ($xml->brief_summary->textblock as $tb) {
            $brief_summary_string = $brief_summary_string . (string)$tb;
        }
        $brief_summary = $brief_summary_string;
        if ($brief_summary == "") {
            $brief_summary = NULL;
        }

        $detailed_description_string = "";
        foreach ($xml->detailed_description->textblock as $tb) {
            $detailed_description_string = $detailed_description_string . (string)$tb;
        }
        $detailed_description = $detailed_description_string;
        if ( $detailed_description == "" ) {
            $detailed_description = NULL;
        }

        $enrollment_type = $xml->enrollment["type"];
        $enrollment = $xml->enrollment;
        $phase = $xml->phase;
        $study_type = $xml->study_type;

        if ( isset ($xml->study_design_info->allocation) ) {
            $allocation = $xml->study_design_info->allocation;
        } else {
            $allocation = NULL;
        }

        if ( isset ($xml->study_design_info->intervention_model) ) {
            $intervention_model = $xml->study_design_info->intervention_model;
        } else {
            $intervention_model = NULL;
        }

        if ( isset ($xml->study_design_info->primary_purpose) ) {
            $primary_purpose = $xml->study_design_info->primary_purpose;
        } else {
            $primary_purpose = NULL;
        }

        if ( isset ($xml->study_design_info->masking) ) {
            $masking = $xml->study_design_info->masking;
        } else {
            $masking = NULL;
        }

        if ($stmt->execute()) {

            $stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
            $stmt2->execute();
            $result = $stmt2->fetchAll();
            $dbh = null;

            $newid = $result[0]['newid'];

            $trial_inserted = TRUE;

        } else {

            $dbh = null;

            echo $filename . " could not be entered into the database<br><hr>";

            $trial_inserted = FALSE;

        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial_inserted) {

        // This is so that the other tables are only populated
        // if there is a corresponding entry in the trials table.

        $interventions = $xml->intervention;

        foreach ($interventions as $intervention) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `interventions` (`trial_id`, `nct_id`, `intervention_type`, `intervention_name`, `description`, `other_name`) VALUES (:trial_id, :nct_id, :intervention_type, :intervention_name, :description, :other_name)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':intervention_type', $intervention_type);
                $stmt->bindParam(':intervention_name', $intervention_name);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':other_name', $other_name);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $intervention_type = $intervention->intervention_type;
                $intervention_name = $intervention->intervention_name;
                $description = $intervention->description;
                $other_name = $intervention->other_name;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $indications = $xml->condition;

        foreach ($indications as $indication) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `indications` (`nct_id`, `trial_id`, `indication`) VALUES (:nct_id, :trial_id, :indication)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':indication', $ind);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $ind = $indication;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $lead_sponsor = $xml->sponsors->lead_sponsor;

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("INSERT INTO `sponsors` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

            $stmt->bindParam(':nct_id', $nct_id);
            $stmt->bindParam(':trial_id', $trial_id);
            $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
            $stmt->bindParam(':agency', $agency);
            $stmt->bindParam(':agency_class', $agency_class);

            $nct_id = $xml->id_info->nct_id;
            $trial_id = $newid;
            $lead_or_collaborator = "lead_sponsor";
            $agency = $lead_sponsor->agency;
            $agency_class = $lead_sponsor->agency_class;

            $stmt->execute();

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

        $sponsors = $xml->sponsors;

        foreach ( $sponsors->collaborator as $sponsor ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `sponsors` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
                $stmt->bindParam(':agency', $agency);
                $stmt->bindParam(':agency_class', $agency_class);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $lead_or_collaborator = "collaborator";
                $agency = $sponsor->agency;
                $agency_class = $sponsor->agency_class;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

	$countries = $xml->location_countries->country;

        foreach ( $countries as $country ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `countries` (`nct_id`, `trial_id`, `country`) VALUES (:nct_id, :trial_id, :country)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':country', $cn);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $cn = $country;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

    }

    if ($trial_inserted) {

        unlink($filename);

    }

}

function first_run () {

    mkdir(ABS_PATH . "dl/");

    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=ghi&lupd_s=12/01/2019&lupd_e=12/31/2019";
    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=ghi&lupd_s=01/01/2020&lupd_e=01/31/2020";
    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=ghi&lupd_s=02/01/2020&lupd_e=02/29/2020";
    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=ghi&lupd_s=03/01/2020&lupd_e=03/31/2020";

    if ( file_put_contents( ABS_PATH . "dl/tmp.zip", file_get_contents($ctg_url)) ) {
        echo "Downloaded zipped folder successfully<br>";
        mkdir (ABS_PATH . "dl/first-run/");

        $zip = new ZipArchive;
        $res = $zip->open( ABS_PATH . "dl/tmp.zip");
        if ($res === TRUE) {
            $zip->extractTo( ABS_PATH . "dl/first-run/");
            unlink ( ABS_PATH . "dl/tmp.zip");
            $zip->close();
            echo "Unzipped XML files successfully<br>";
        } else {
            return FALSE;
        }

    } else {
        echo "Failed to download zipped folder<br>";
        return FALSE;
    }

    foreach ( glob ( ABS_PATH . "dl/first-run/*") as $filename ) {

        process_xml_file ( $filename );

    }

    rmdir(ABS_PATH . "dl/first-run/");

    return TRUE;

}

function update_from_ctg ( $days ) {

    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=ghi&lupd_s=" . date("m/d/Y", time() - 86400 * $days);

    echo $ctg_url . "<br>";

    if ( file_put_contents( ABS_PATH . "dl/tmp.zip", file_get_contents($ctg_url)) ) {
        echo "Downloaded zipped folder successfully<br>";
        mkdir (ABS_PATH . "dl/" . date("Y-m-d") . "/");

        $zip = new ZipArchive;
        $res = $zip->open( ABS_PATH . "dl/tmp.zip");
        if ($res === TRUE) {
            $zip->extractTo( ABS_PATH . "dl/" . date("Y-m-d") . "/");
            unlink ( ABS_PATH . "dl/tmp.zip");
            $zip->close();
            echo "Unzipped XML files successfully<br>";
        } else {
            return FALSE;
        }

    } else {
        echo "Failed to download zipped folder<br>";
        return FALSE;
    }

    foreach ( glob ( ABS_PATH . "dl/" . date("Y-m-d") . "/*") as $filename ) {

        process_xml_file ( $filename );

    }

    rmdir(ABS_PATH . "dl/" . date("Y-m-d") . "/");

    return TRUE;

}

function get_display_table_entries () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `include` = 1 ORDER BY `last_update_submitted` DESC;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_admin_table_entries () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        // $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `include` IS NULL ORDER BY `last_update_submitted` DESC LIMIT 20;");
        // $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `covid19_explicit` = 1 AND `restartexpected` IS NULL ORDER BY `last_update_submitted` DESC LIMIT 20;");
        // $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `include` = 1 AND `date_stopped` IS NULL ORDER BY `last_update_submitted` DESC LIMIT 20;");
	$stmt = $dbh->prepare("SELECT * FROM `trials` WHERE (`include` IS NULL) OR (`include` = 1 AND `date_stopped` IS NULL) OR (`include` = 1 AND `covid19_explicit` IS NULL) OR (`covid19_explicit` = 1 AND `restartexpected` IS NULL) ORDER BY `last_update_submitted` DESC LIMIT 20;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_admin_comparator_table_entries () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `include` IS NULL AND `also_in_covid_arm` IS NULL ORDER BY `last_update_submitted` ASC LIMIT 20;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_trial_details ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }
}

function get_trial_sponsors ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `sponsors` WHERE `trial_id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_trial_interventions ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `interventions` WHERE `trial_id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_trial_indications ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `indications` WHERE `trial_id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_include ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['include'] === NULL) {
        $new_include = 1;
    } else {
        switch ($trial['include']) {
            case 0:
                $new_include = NULL;
                break;
            case 1:
                $new_include = 0;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials` SET `include` = :newinclude WHERE `id` = :tid;");

        $stmt->bindParam(':newinclude', $ni);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $ni = $new_include;

        if ($stmt->execute()) {

            if ($new_include === NULL) {
                $display_include = "-";
            } else {
                switch ($new_include) {
                    case 0:
                        $display_include = "No";
                        break;
                    case 1:
                        $display_include = "Yes";
                        break;
                }
            }

            // update include_update in case i make a mistake while coding
            // lol can you imagine me making a mistake ha ha
            // O_o

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("UPDATE `trials` SET `include_updated` = NOW() WHERE `id` = :tid;");

                $stmt->bindParam(':tid', $tid);

                $tid = $trial_id;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

            return $display_include;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function set_comparator_include ($trial_id, $newval) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include` = :newval WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':newval', $nv);

        $tid = $trial_id;
        $nv = $newval;

        if ($stmt->execute()) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include_updated` = NOW() WHERE `id` = :tid;");

                $stmt->bindParam(':tid', $tid);

                $tid = $trial_id;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

            return $nv;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_comparator_include ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['include'] === NULL) {
        $new_include = 1;
    } else {
        switch ($trial['include']) {
            case 0:
                $new_include = NULL;
                break;
            case 1:
                $new_include = 0;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include` = :newinclude WHERE `id` = :tid;");

        $stmt->bindParam(':newinclude', $ni);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $ni = $new_include;

        if ($stmt->execute()) {

            if ($new_include === NULL) {
                $display_include = "-";
            } else {
                switch ($new_include) {
                    case 0:
                        $display_include = "No";
                        break;
                    case 1:
                        $display_include = "Yes";
                        break;
                }
            }

            // update include_update in case i make a mistake while coding
            // lol can you imagine me making a mistake ha ha
            // O_o

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include_updated` = NOW() WHERE `id` = :tid;");

                $stmt->bindParam(':tid', $tid);

                $tid = $trial_id;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

            return $display_include;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_comparator_statusws ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['status_when_stopped'] === NULL) {
        $new_statusws = "S";
    } else {
        switch ($trial['status_when_stopped']) {
            case "S":
                $new_statusws = "T";
                break;
            case "T":
                $new_statusws = "W";
                break;
            case "W":
                $new_statusws = NULL;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `status_when_stopped` = :newstatusws WHERE `id` = :tid;");

        $stmt->bindParam(':newstatusws', $nsws);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $nsws = $new_statusws;

        if ($stmt->execute()) {

            if ($new_statusws === NULL) {
                $display_statusws = "-";
            } else {
                switch ($new_statusws) {
                    case "S":
                        $display_statusws = "Suspended";
                        break;
                    case "T":
                        $display_statusws = "Terminated";
                        break;
                    case "W":
                        $display_statusws = "Withdrawn";
                        break;
                }
            }

            // update include_update in case i make a mistake while coding
            // lol can you imagine me making a mistake ha ha
            // O_o

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include_updated` = NOW() WHERE `id` = :tid;");

                $stmt->bindParam(':tid', $tid);

                $tid = $trial_id;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

            return $display_statusws;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_covid ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['covid19_explicit'] === NULL) {
        $new_covid = 1;
    } else {
        switch ($trial['covid19_explicit']) {
            case 0:
                $new_covid = NULL;
                break;
            case 1:
                $new_covid = 0;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials` SET `covid19_explicit` = :newcovid WHERE `id` = :tid;");

        $stmt->bindParam(':newcovid', $nc);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $nc = $new_covid;

        if ($stmt->execute()) {

            if ($new_covid === NULL) {
                $display_covid = "-";
            } else {
                switch ($new_covid) {
                    case 0:
                        $display_covid = "No";
                        break;
                    case 1:
                        $display_covid = "Yes";
                        break;
                }
            }

            return $display_covid;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_restartexpected ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['restartexpected'] === NULL) {
        $new_re = 1;
    } else {
        switch ($trial['restartexpected']) {
            case 0:
                $new_re = NULL;
                break;
            case 1:
                $new_re = 0;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials` SET `restartexpected` = :newre WHERE `id` = :tid;");

        $stmt->bindParam(':newre', $nre);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $nre = $new_re;

        if ($stmt->execute()) {

            if ($new_re === NULL) {
                $display_re = "-";
            } else {
                switch ($new_re) {
                    case 0:
                        $display_re = "No";
                        break;
                    case 1:
                        $display_re = "Yes";
                        break;
                }
            }

            return $display_re;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_comparator_restarted6 ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        $stmt->execute();

        $result = $stmt->fetchAll();

        $trial = $result[0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial['restarted6'] === NULL) {
        $new_r6 = 1;
    } else {
        switch ($trial['restarted6']) {
            case 0:
                $new_r6 = NULL;
                break;
            case 1:
                $new_r6 = 0;
                break;
        }
    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `restarted6` = :newrestarted WHERE `id` = :tid;");

        $stmt->bindParam(':newrestarted', $nr6);
        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;
        $nr6 = $new_r6;

        if ($stmt->execute()) {

            if ($new_r6 === NULL) {
                $display_r6 = "-";
            } else {
                switch ($new_r6) {
                    case 0:
                        $display_r6 = "No";
                        break;
                    case 1:
                        $display_r6 = "Yes";
                        break;
                }
            }

            return $display_r6;
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_datestopped ($trial_id, $newdate) {

    if ( strtotime ($newdate) ) {
        // If the new date is well-formed

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("UPDATE `trials` SET `date_stopped` = :newdate WHERE `id` = :tid;");

            $stmt->bindParam(':newdate', $nd);
            $stmt->bindParam(':tid', $tid);

            $tid = $trial_id;
            $nd = date("Y-m-d", strtotime($newdate));

            if ($stmt->execute()) {
                return $nd;
            } else {
                return "MySQL Error";
            }

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

    } else {
        // The new date is not well-formed
        return "nwf";
    }

}

function update_comparator_datestopped ($trial_id, $newdate) {

    if ( strtotime ($newdate) ) {
        // If the new date is well-formed

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `date_stopped` = :newdate WHERE `id` = :tid;");

            $stmt->bindParam(':newdate', $nd);
            $stmt->bindParam(':tid', $tid);

            $tid = $trial_id;
            $nd = date("Y-m-d", strtotime($newdate));

            if ($stmt->execute()) {
                return $nd;
            } else {
                return "MySQL Error";
            }

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

    } else {
        // The new date is not well-formed
        return "nwf";
    }

}

function update_comparator_daterestarted ($trial_id, $newdate) {

    if ( strtotime ($newdate) ) {
        // If the new date is well-formed

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `restartdate` = :newdate WHERE `id` = :tid;");

            $stmt->bindParam(':newdate', $nd);
            $stmt->bindParam(':tid', $tid);

            $tid = $trial_id;
            $nd = date("Y-m-d", strtotime($newdate));

            if ($stmt->execute()) {
                return $nd;
            } else {
                return "MySQL Error";
            }

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

    } else {
        // The new date is not well-formed
        return "nwf";
    }

}

function clear_datestopped ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials` SET `date_stopped` = NULL WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        if ($stmt->execute()) {
            return "-";
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function clear_comparator_datestopped ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `date_stopped` = NULL WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        if ($stmt->execute()) {
            return "-";
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function clear_comparator_daterestarted ($trial_id) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `restartdate` = NULL WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);

        $tid = $trial_id;

        if ($stmt->execute()) {
            return "-";
        } else {
            return "MySQL Error";
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function count_comparator_trials_not_assessed_for_inclusion () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT COUNT(*) FROM `trials_comparator` WHERE `include` IS NULL and `also_in_covid_arm` IS NULL;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result[0][0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }
}

function count_trials_not_assessed_for_inclusion () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT COUNT(*) FROM `trials` WHERE `include` IS NULL;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result[0][0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }
}

function get_last_update () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE `include` = 1 ORDER BY `include_updated` DESC LIMIT 1;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result[0]['include_updated'];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function export_covid_trials () {

    //exec ( "mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT *, (SELECT agency_class FROM sponsors WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors.trial_id = trials.id LIMIT 1) as sponsor_agency_class FROM trials WHERE include = 1 ORDER BY last_update_submitted DESC;\" > " . ABS_PATH . "published/covid-arm-" . date("Y-m-d") . ".tsv" );
    // exec ( "mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT nct_id, primary_completion_date, enrollment_type, enrollment, phase, study_type, allocation, intervention_model, primary_purpose, masking, datestopped_2017 as date_stopped, status_2017 as status_when_stopped, restartdate_2017 as restartdate, cancer_indication, neuro_indication, cv_indication, pain_indication, (SELECT agency_class FROM sponsors_comparator WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors_comparator.trial_id = trials_comparator.id LIMIT 1) as sponsor_agency_class FROM trials_comparator WHERE include_2017 = 1 ORDER BY nct_id;\" > " . ABS_PATH . "published/comparator-arm-" . date("Y-m-d") . ".tsv" );
    //exec ( "mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT interventions.nct_id, interventions.intervention_type, interventions.intervention_name, interventions.description, interventions.other_name, interventions.curated, trials.cancer_indication, trials.neuro_indication, trials.cv_indication, trials.pain_indication FROM interventions, trials WHERE interventions.intervention_name NOT LIKE '%Placebo%' AND (interventions.intervention_type = 'Drug' OR interventions.intervention_type = 'Biological') AND interventions.trial_id = trials.id AND trials.covid19_explicit = 1;\" > " . ABS_PATH . "published/druglist-" . date("Y-m-d") . ".tsv" );

    // (SELECT agency_class FROM sponsors WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors.trial_id = trials.id LIMIT 1) as sponsor_agency_class
    // (SELECT agency_class FROM sponsors_comparator WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors_comparator.trial_id = trials_comparator.id LIMIT 1) as sponsor_agency_class

    echo "<p>mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT *, (SELECT agency_class FROM sponsors WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors.trial_id = trials.id LIMIT 1) as sponsor_agency_class FROM trials WHERE include = 1 ORDER BY last_update_submitted DESC;\" > " . ABS_PATH . "published/covid-arm-" . date("Y-m-d") . ".tsv</p>";

    echo "<p>mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT nct_id, primary_completion_date, enrollment_type, enrollment, phase, study_type, allocation, intervention_model, primary_purpose, masking, datestopped_2017 as date_stopped, status_2017 as status_when_stopped, restartdate_2017 as restartdate, cancer_indication, neuro_indication, cv_indication, pain_indication, (SELECT agency_class FROM sponsors_comparator WHERE lead_or_collaborator = 'lead_sponsor' AND sponsors_comparator.trial_id = trials_comparator.id LIMIT 1) as sponsor_agency_class FROM trials_comparator WHERE include_2017 = 1 ORDER BY nct_id;\" > " . ABS_PATH . "published/comparator-arm-" . date("Y-m-d") . ".tsv</p>";

    echo "<p>mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT interventions.nct_id, interventions.intervention_type, interventions.intervention_name, interventions.description, interventions.other_name, interventions.curated, trials.cancer_indication, trials.neuro_indication, trials.cv_indication, trials.pain_indication FROM interventions, trials WHERE interventions.intervention_name NOT LIKE '%Placebo%' AND (interventions.intervention_type = 'Drug' OR interventions.intervention_type = 'Biological') AND interventions.trial_id = trials.id AND trials.covid19_explicit = 1;\" > " . ABS_PATH . "published/druglist-" . date("Y-m-d") . ".tsv</p>";

    return TRUE;

}

function comparator_download ($start, $end) {

    //$ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=12/15/2018&lupd_e=12/31/2018";
    //$ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=01/01/2019&lupd_e=01/14/2019";
    //$ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=01/15/2019&lupd_e=01/31/2019";
    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=" . date("m/d/Y", strtotime($start)) . "&lupd_e=" . date("m/d/Y", strtotime($end));

    if ( file_put_contents( ABS_PATH . "dl/tmp.zip", file_get_contents($ctg_url)) ) {
        echo "Downloaded zipped folder successfully<br>";
        mkdir (ABS_PATH . "dl/comparator/");

        $zip = new ZipArchive;
        $res = $zip->open( ABS_PATH . "dl/tmp.zip");
        if ($res === TRUE) {
            $zip->extractTo( ABS_PATH . "dl/comparator/");
            unlink ( ABS_PATH . "dl/tmp.zip");
            $zip->close();
            echo "Unzipped XML files successfully<br>";
        } else {
            return FALSE;
        }

    } else {
        echo "Failed to download zipped folder<br>";
        return FALSE;
    }

    echo count (glob ( ABS_PATH . "dl/comparator/*")) . " files<br><br>";

    foreach ( glob ( ABS_PATH . "dl/comparator/*") as $filename ) {

        process_comparator_xml_file ( $filename );

    }

    rmdir(ABS_PATH . "dl/comparator/");

    return TRUE;

}

function denominator_download ($start, $end) {

    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=" . date("m/d/Y", strtotime($start)) . "&lupd_e=" . date("m/d/Y", strtotime($end));

    // For downloading all the currently open trials that were last changed before the comparator period:
    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?recrs=adf&lupd_e=11/30/2017";

    if ( file_put_contents( ABS_PATH . "dl/tmp.zip", file_get_contents($ctg_url)) ) {
        echo "Downloaded zipped folder successfully<br>";
        mkdir (ABS_PATH . "dl/denominator/");

        $zip = new ZipArchive;
        $res = $zip->open( ABS_PATH . "dl/tmp.zip");
        if ($res === TRUE) {
            $zip->extractTo( ABS_PATH . "dl/denominator/");
            unlink ( ABS_PATH . "dl/tmp.zip");
            $zip->close();
            echo "Unzipped XML files successfully<br>";
        } else {
            return FALSE;
        }

    } else {
        echo "Failed to download zipped folder<br>";
        return FALSE;
    }

    echo count (glob ( ABS_PATH . "dl/denominator/*")) . " files<br><br>";

    foreach ( glob ( ABS_PATH . "dl/denominator/*") as $filename ) {

        process_denominator_xml_file ( $filename );

    }

    rmdir(ABS_PATH . "dl/denominator/");

    return TRUE;
}

function process_denominator_xml_file ( $filename ) {

    $xml = simplexml_load_file($filename);

    if (! $xml ) {

        echo "The XML file didn't load<br>";

    } else {

        // echo $xml->id_info->nct_id . "<br>";

    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("INSERT INTO `trials_denominator` (`nct_id`, `overall_status`, `why_stopped`, `primary_completion_date`, `primary_completion_date_type`, `last_update_submitted`, `brief_title`, `official_title`, `brief_summary`, `detailed_description`, `enrollment_type`, `enrollment`, `phase`, `study_type`, `allocation`, `intervention_model`, `primary_purpose`, `masking`) VALUES (:nct_id, :overall_status, :why_stopped, :primary_completion_date, :primary_completion_date_type, :last_update_submitted, :brief_title, :official_title, :brief_summary, :detailed_description, :enrollment_type, :enrollment, :phase, :study_type, :allocation, :intervention_model, :primary_purpose, :masking)");

        $stmt->bindParam(':nct_id', $nct_id);
        $stmt->bindParam(':overall_status', $overall_status);
        $stmt->bindParam(':why_stopped', $why_stopped);
        $stmt->bindParam(':primary_completion_date', $primary_completion_date);
        $stmt->bindParam(':primary_completion_date_type', $primary_completion_date_type);
        $stmt->bindParam(':last_update_submitted', $last_update_submitted);
        $stmt->bindParam(':brief_title', $brief_title);
        $stmt->bindParam(':official_title', $official_title);
        $stmt->bindParam(':brief_summary', $brief_summary);
        $stmt->bindParam(':detailed_description', $detailed_description);
        $stmt->bindParam(':enrollment_type', $enrollment_type);
        $stmt->bindParam(':enrollment', $enrollment);
        $stmt->bindParam(':phase', $phase);
        $stmt->bindParam(':study_type', $study_type);
        $stmt->bindParam(':allocation', $allocation);
        $stmt->bindParam(':intervention_model', $intervention_model);
        $stmt->bindParam(':primary_purpose', $primary_purpose);
        $stmt->bindParam(':masking', $masking);

        $nct_id = $xml->id_info->nct_id;
        $overall_status = $xml->overall_status;

        if ($xml->why_stopped != "") {
            $why_stopped = $xml->why_stopped;
        } else {
            $why_stopped = NULL;
        }

        if ( isset ($xml->primary_completion_date) ) {
            $primary_completion_date = date("Y-m-d", strtotime($xml->primary_completion_date));
            $primary_completion_date_type = $xml->primary_completion_date["type"];
        } else {
            $primary_completion_date = NULL;
            $primary_completion_date_type = NULL;
        }

        $last_update_submitted = date("Y-m-d", strtotime($xml->last_update_submitted));
        $brief_title = $xml->brief_title;
        $official_title = $xml->official_title;

        $brief_summary_string = "";
        foreach ($xml->brief_summary->textblock as $tb) {
            $brief_summary_string = $brief_summary_string . (string)$tb;
        }
        $brief_summary = $brief_summary_string;
        if ($brief_summary == "") {
            $brief_summary = NULL;
        }

        $detailed_description_string = "";
        foreach ($xml->detailed_description->textblock as $tb) {
            $detailed_description_string = $detailed_description_string . (string)$tb;
        }
        $detailed_description = $detailed_description_string;
        if ( $detailed_description == "" ) {
            $detailed_description = NULL;
        }

        $enrollment_type = $xml->enrollment["type"];
        $enrollment = $xml->enrollment;
        $phase = $xml->phase;
        $study_type = $xml->study_type;

        if ( isset ($xml->study_design_info->allocation) ) {
            $allocation = $xml->study_design_info->allocation;
        } else {
            $allocation = NULL;
        }

        if ( isset ($xml->study_design_info->intervention_model) ) {
            $intervention_model = $xml->study_design_info->intervention_model;
        } else {
            $intervention_model = NULL;
        }

        if ( isset ($xml->study_design_info->primary_purpose) ) {
            $primary_purpose = $xml->study_design_info->primary_purpose;
        } else {
            $primary_purpose = NULL;
        }

        if ( isset ($xml->study_design_info->masking) ) {
            $masking = $xml->study_design_info->masking;
        } else {
            $masking = NULL;
        }

        if ($stmt->execute()) {

            $stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
            $stmt2->execute();
            $result = $stmt2->fetchAll();
            $dbh = null;

            $newid = $result[0]['newid'];

            $trial_inserted = TRUE;

        } else {

            $dbh = null;

            echo $filename . " could not be entered into the database<br><hr>";

            $trial_inserted = FALSE;

        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial_inserted) {

        // This is so that the other tables are only populated
        // if there is a corresponding entry in the trials table.

        $interventions = $xml->intervention;

        foreach ($interventions as $intervention) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `interventions_denominator` (`trial_id`, `nct_id`, `intervention_type`, `intervention_name`, `description`, `other_name`) VALUES (:trial_id, :nct_id, :intervention_type, :intervention_name, :description, :other_name)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':intervention_type', $intervention_type);
                $stmt->bindParam(':intervention_name', $intervention_name);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':other_name', $other_name);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $intervention_type = $intervention->intervention_type;
                $intervention_name = $intervention->intervention_name;
                $description = $intervention->description;
                $other_name = $intervention->other_name;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $indications = $xml->condition;

        foreach ($indications as $indication) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `indications_denominator` (`nct_id`, `trial_id`, `indication`) VALUES (:nct_id, :trial_id, :indication)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':indication', $ind);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $ind = $indication;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $lead_sponsor = $xml->sponsors->lead_sponsor;

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("INSERT INTO `sponsors_denominator` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

            $stmt->bindParam(':nct_id', $nct_id);
            $stmt->bindParam(':trial_id', $trial_id);
            $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
            $stmt->bindParam(':agency', $agency);
            $stmt->bindParam(':agency_class', $agency_class);

            $nct_id = $xml->id_info->nct_id;
            $trial_id = $newid;
            $lead_or_collaborator = "lead_sponsor";
            $agency = $lead_sponsor->agency;
            $agency_class = $lead_sponsor->agency_class;

            $stmt->execute();

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

        $sponsors = $xml->sponsors;

        foreach ( $sponsors->collaborator as $sponsor ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `sponsors_denominator` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
                $stmt->bindParam(':agency', $agency);
                $stmt->bindParam(':agency_class', $agency_class);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $lead_or_collaborator = "collaborator";
                $agency = $sponsor->agency;
                $agency_class = $sponsor->agency_class;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

        $countries = $xml->location_countries->country;

        foreach ( $countries as $country ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `countries_denominator` (`nct_id`, `trial_id`, `country`) VALUES (:nct_id, :trial_id, :country)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':country', $cn);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $cn = $country;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

    }

    if ($trial_inserted) {

        unlink($filename);

    }
}

function process_comparator_xml_file ( $filename ) {

    $xml = simplexml_load_file($filename);

    if (! $xml ) {

        echo "The XML file didn't load<br>";

    } else {

        // echo $xml->id_info->nct_id . "<br>";

    }

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("INSERT INTO `trials_comparator` (`nct_id`, `overall_status`, `why_stopped`, `primary_completion_date`, `primary_completion_date_type`, `last_update_submitted`, `brief_title`, `official_title`, `brief_summary`, `detailed_description`, `enrollment_type`, `enrollment`, `phase`, `study_type`, `allocation`, `intervention_model`, `primary_purpose`, `masking`) VALUES (:nct_id, :overall_status, :why_stopped, :primary_completion_date, :primary_completion_date_type, :last_update_submitted, :brief_title, :official_title, :brief_summary, :detailed_description, :enrollment_type, :enrollment, :phase, :study_type, :allocation, :intervention_model, :primary_purpose, :masking)");

        $stmt->bindParam(':nct_id', $nct_id);
        $stmt->bindParam(':overall_status', $overall_status);
        $stmt->bindParam(':why_stopped', $why_stopped);
        $stmt->bindParam(':primary_completion_date', $primary_completion_date);
        $stmt->bindParam(':primary_completion_date_type', $primary_completion_date_type);
        $stmt->bindParam(':last_update_submitted', $last_update_submitted);
        $stmt->bindParam(':brief_title', $brief_title);
        $stmt->bindParam(':official_title', $official_title);
        $stmt->bindParam(':brief_summary', $brief_summary);
        $stmt->bindParam(':detailed_description', $detailed_description);
        $stmt->bindParam(':enrollment_type', $enrollment_type);
        $stmt->bindParam(':enrollment', $enrollment);
        $stmt->bindParam(':phase', $phase);
        $stmt->bindParam(':study_type', $study_type);
        $stmt->bindParam(':allocation', $allocation);
        $stmt->bindParam(':intervention_model', $intervention_model);
        $stmt->bindParam(':primary_purpose', $primary_purpose);
        $stmt->bindParam(':masking', $masking);

        $nct_id = $xml->id_info->nct_id;
        $overall_status = $xml->overall_status;

        if ($xml->why_stopped != "") {
            $why_stopped = $xml->why_stopped;
        } else {
            $why_stopped = NULL;
        }

        if ( isset ($xml->primary_completion_date) ) {
            $primary_completion_date = date("Y-m-d", strtotime($xml->primary_completion_date));
            $primary_completion_date_type = $xml->primary_completion_date["type"];
        } else {
            $primary_completion_date = NULL;
            $primary_completion_date_type = NULL;
        }

        $last_update_submitted = date("Y-m-d", strtotime($xml->last_update_submitted));
        $brief_title = $xml->brief_title;
        $official_title = $xml->official_title;

        $brief_summary_string = "";
        foreach ($xml->brief_summary->textblock as $tb) {
            $brief_summary_string = $brief_summary_string . (string)$tb;
        }
        $brief_summary = $brief_summary_string;
        if ($brief_summary == "") {
            $brief_summary = NULL;
        }

        $detailed_description_string = "";
        foreach ($xml->detailed_description->textblock as $tb) {
            $detailed_description_string = $detailed_description_string . (string)$tb;
        }
        $detailed_description = $detailed_description_string;
        if ( $detailed_description == "" ) {
            $detailed_description = NULL;
        }

        $enrollment_type = $xml->enrollment["type"];
        $enrollment = $xml->enrollment;
        $phase = $xml->phase;
        $study_type = $xml->study_type;

        if ( isset ($xml->study_design_info->allocation) ) {
            $allocation = $xml->study_design_info->allocation;
        } else {
            $allocation = NULL;
        }

        if ( isset ($xml->study_design_info->intervention_model) ) {
            $intervention_model = $xml->study_design_info->intervention_model;
        } else {
            $intervention_model = NULL;
        }

        if ( isset ($xml->study_design_info->primary_purpose) ) {
            $primary_purpose = $xml->study_design_info->primary_purpose;
        } else {
            $primary_purpose = NULL;
        }

        if ( isset ($xml->study_design_info->masking) ) {
            $masking = $xml->study_design_info->masking;
        } else {
            $masking = NULL;
        }

        if ($stmt->execute()) {

            $stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
            $stmt2->execute();
            $result = $stmt2->fetchAll();
            $dbh = null;

            $newid = $result[0]['newid'];

            $trial_inserted = TRUE;

        } else {

            $dbh = null;

            echo $filename . " could not be entered into the database<br><hr>";

            $trial_inserted = FALSE;

        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

    if ($trial_inserted) {

        // This is so that the other tables are only populated
        // if there is a corresponding entry in the trials table.

        $interventions = $xml->intervention;

        foreach ($interventions as $intervention) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `interventions_comparator` (`trial_id`, `nct_id`, `intervention_type`, `intervention_name`, `description`, `other_name`) VALUES (:trial_id, :nct_id, :intervention_type, :intervention_name, :description, :other_name)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':intervention_type', $intervention_type);
                $stmt->bindParam(':intervention_name', $intervention_name);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':other_name', $other_name);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $intervention_type = $intervention->intervention_type;
                $intervention_name = $intervention->intervention_name;
                $description = $intervention->description;
                $other_name = $intervention->other_name;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $indications = $xml->condition;

        foreach ($indications as $indication) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `indications_comparator` (`nct_id`, `trial_id`, `indication`) VALUES (:nct_id, :trial_id, :indication)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':indication', $ind);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $ind = $indication;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }
        }

        $lead_sponsor = $xml->sponsors->lead_sponsor;

        try {

            $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $dbh->prepare("INSERT INTO `sponsors_comparator` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

            $stmt->bindParam(':nct_id', $nct_id);
            $stmt->bindParam(':trial_id', $trial_id);
            $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
            $stmt->bindParam(':agency', $agency);
            $stmt->bindParam(':agency_class', $agency_class);

            $nct_id = $xml->id_info->nct_id;
            $trial_id = $newid;
            $lead_or_collaborator = "lead_sponsor";
            $agency = $lead_sponsor->agency;
            $agency_class = $lead_sponsor->agency_class;

            $stmt->execute();

        }

        catch (PDOException $e) {

            echo $e->getMessage();

        }

        $sponsors = $xml->sponsors;

        foreach ( $sponsors->collaborator as $sponsor ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `sponsors_comparator` (`nct_id`, `trial_id`, `lead_or_collaborator`, `agency`, `agency_class`) VALUES (:nct_id, :trial_id, :lead_or_collaborator, :agency, :agency_class)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':lead_or_collaborator', $lead_or_collaborator);
                $stmt->bindParam(':agency', $agency);
                $stmt->bindParam(':agency_class', $agency_class);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $lead_or_collaborator = "collaborator";
                $agency = $sponsor->agency;
                $agency_class = $sponsor->agency_class;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

        $countries = $xml->location_countries->country;

        foreach ( $countries as $country ) {

            try {

                $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $stmt = $dbh->prepare("INSERT INTO `countries_comparator` (`nct_id`, `trial_id`, `country`) VALUES (:nct_id, :trial_id, :country)");

                $stmt->bindParam(':nct_id', $nct_id);
                $stmt->bindParam(':trial_id', $trial_id);
                $stmt->bindParam(':country', $cn);

                $nct_id = $xml->id_info->nct_id;
                $trial_id = $newid;
                $cn = $country;

                $stmt->execute();

            }

            catch (PDOException $e) {

                echo $e->getMessage();

            }

        }

    }

    if ($trial_inserted) {

        unlink($filename);

    }
}

function get_unscreened_2017_cohort_trials ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `include_2017` IS NULL ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_unscreened_denominator_trials ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_denominator` WHERE `include_c19` IS NULL OR `include_comp` IS NULL ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_unextracted_2017_cohort_trials ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `include_2017` = 1 AND `restarted_2017` IS NULL ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_excluded_and_non_double_checked_comparators ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE include = 0 AND `double_check_include` IS NULL AND `also_in_covid_arm` IS NULL ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function mark_for_double_checking ( $trial_id, $new_value ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `double_check_include` = :nv WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':nv', $nv);

        $tid = $trial_id;
        $nv = $new_value;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function screen2017cohort ( $trial_id, $include, $datestopped, $status ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `include_2017` = :in, `datestopped_2017` = :ds, `status_2017` = :st WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':in', $in);
        $stmt->bindParam(':ds', $ds);
        $stmt->bindParam(':st', $st);

        $tid = $trial_id;
        $in = $include;
        $ds = $datestopped;
        $st = $status;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function extract2017cohort ( $trial_id, $restarted, $restartdate ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `restarted_2017` = :rs, `restartdate_2017` = :rd WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':rs', $rs);
        $stmt->bindParam(':rd', $rd);

        $tid = $trial_id;
        $rs = $restarted;
        $rd = $restartdate;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_included_comparator_trials_with_indications_not_specified ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials_comparator` WHERE `include_2017` = 1 AND (`cancer_indication` IS NULL OR `neuro_indication` IS NULL OR `cv_indication` IS NULL OR `pain_indication` IS NULL) ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_included_c19arm_trials_with_indications_not_specified ( $limit ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT * FROM `trials` WHERE include = 1 AND (`cancer_indication` IS NULL OR `neuro_indication` IS NULL OR `cv_indication` IS NULL OR `pain_indication` IS NULL OR `c19_indication` IS NULL) ORDER BY `last_update_submitted` ASC LIMIT " . $limit . ";");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function nct_in_indication ( $nct_id, $indication ) {

    $ctg_url = "https://clinicaltrials.gov/ct2/results/download_studies?cond=" . $indication . "&term=" . $nct_id;

    if ( file_put_contents( ABS_PATH . "dl/tmp.zip", file_get_contents($ctg_url)) ) {
        return TRUE;
    } else {
        return FALSE;
    }

}

function update_c19arm_indications ($trial_id, $cancer, $neuro, $cardio, $pain, $c19) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials` SET `cancer_indication` = :ca, `neuro_indication` = :ne, `cv_indication` = :cv, `pain_indication` = :pa, `c19_indication` = :co WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':ca', $ca);
        $stmt->bindParam(':ne', $ne);
        $stmt->bindParam(':cv', $cv);
        $stmt->bindParam(':pa', $pa);
        $stmt->bindParam(':co', $co);

        $tid = $trial_id;
        $ca = $cancer;
        $ne = $neuro;
        $cv = $cardio;
        $pa = $pain;
        $co = $c19;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_comparator_indications ($trial_id, $cancer, $neuro, $cardio, $pain) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_comparator` SET `cancer_indication` = :ca, `neuro_indication` = :ne, `cv_indication` = :cv, `pain_indication` = :pa WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':ca', $ca);
        $stmt->bindParam(':ne', $ne);
        $stmt->bindParam(':cv', $cv);
        $stmt->bindParam(':pa', $pa);

        $tid = $trial_id;
        $ca = $cancer;
        $ne = $neuro;
        $cv = $cardio;
        $pa = $pain;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function update_denominator_indications ($trial_id, $cancer, $neuro, $cardio, $pain) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `trials_denominator` SET `cancer_indication` = :ca, `neuro_indication` = :ne, `cv_indication` = :cv, `pain_indication` = :pa WHERE `id` = :tid;");

        $stmt->bindParam(':tid', $tid);
        $stmt->bindParam(':ca', $ca);
        $stmt->bindParam(':ne', $ne);
        $stmt->bindParam(':cv', $cv);
        $stmt->bindParam(':pa', $pa);

        $tid = $trial_id;
        $ca = $cancer;
        $ne = $neuro;
        $cv = $cardio;
        $pa = $pain;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_c19_explicit_count () {
    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT COUNT(*) FROM `trials` WHERE `covid19_explicit` = 1;");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result[0][0];

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function get_drug_curation_table () {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("SELECT `id`, `nct_id`, `intervention_name` as `int_name`, `description`, `other_name`, (SELECT `curated` FROM `interventions` WHERE `intervention_name` = `int_name` AND `curated` IS NOT NULL LIMIT 1) as `suggestion` FROM `interventions` WHERE `curated` IS NULL AND (`intervention_type` = 'Drug' OR `intervention_type` = 'Biological') AND `intervention_name` NOT LIKE '%placebo%' AND `trial_id` IN (SELECT `id` FROM `trials` WHERE `covid19_explicit` = 1) LIMIT 10");

        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function save_druglist_curation ( $row_id, $new_value ) {

    try {

        $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $stmt = $dbh->prepare("UPDATE `interventions` SET `curated` = :nv WHERE `id` = :rid;");

        $stmt->bindParam(':rid', $rid);
        $stmt->bindParam(':nv', $nv);

        $rid = $row_id;
        $nv = $new_value;

        if ($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    catch (PDOException $e) {

        echo $e->getMessage();

    }

}

function send_message ( $email, $message ) {

    $body = "Sender: " . $email . "\n\n";

    $body = $body . "Message:\n\n";

    $body = $body . $message . "\n\n";

    $body = $body . "Sent " . date("Y-m-d H:i");

    $headers = array (
        'From' => EMAIL
    );

    if (mail(
        EMAIL,
        "Message posted to covid19.bgcarlisle.com from " . $email . " on " . date("Y-m-d H:i"),
        $body,
        $headers
    )) {
        return TRUE;
    } else {
        return FALSE;
    }

}

?>
